=Admin=
- beheer events (ajax om artiesten en venues te vullen)
	- detailform: artiest, tour en venue via ajax
	- publish in overzicht
- beheer artiesten
	- aantal events in overzicht
- beheer venues
- beheer tours
- shortcodes(?) om events op een pagina te zetten
- sjabloon(?)

=Frontend=
- pagina met events (standaard)
- pagina met events (artiest)
- pagina met events (venue)
- pagina met events (tour)

- widget met events(?)