<?php
/*
Plugin Name: WipeOut! eventplugin
Plugin URI: http://www.wipeout.nu/wordpress
Description: Plugin, ontwikkeld voor bands en artiesten. Te gebruiken voor eventmanagement.
Version: 1.0
Author: WipeOut! webservices
Author URI: http://www.wipeout.nu/
Text Domain: wow_events
License: GPL2
*/

	require_once( plugin_dir_path( __FILE__ ).'lib/class.php' );
	require_once( plugin_dir_path( __FILE__ ).'lib/install.php' );
	require_once( plugin_dir_path( __FILE__ ).'lib/admin-events.php' );

	// initieer database
	register_activation_hook( __FILE__,'wow_events_install' );

	$eventtable       = $wpdb->prefix . "wowevents_events";
	$artisttable      = $wpdb->prefix . "wowevents_artists";
	$venuetable       = $wpdb->prefix . "wowevents_venues";
	$tourtable        = $wpdb->prefix . "wowevents_tour";
	$toureventtable   = $wpdb->prefix . "wowevents_tour_event";
	global $eventtable;
	global $artisttable;
	global $venuetable;
	global $tourtable;
	global $toureventtable;

	/**
	 * adminmenuoptie toevoegen aan admin menu
	 */
	add_action('admin_menu', 'wow_events_menu');
	wp_register_script( 'tablesorter', plugin_dir_url( __FILE__ ) .'js/jquery.tablesorter.min.js');

function wow_events_menu()
{
	/**
	 * - voeg een menuoptie toe aan admin menu
	 * - voeg javascript en callbackscripts toe voor admin
	 * - voeg stylesheets toe voor admin
	 */
	//add_action('admin_footer', 'wow_events_javascript');
	//add_action('admin_footer', 'wow_events_sets_javascript');
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_script('jquery-ui-sortable');
	wp_enqueue_script( 'tablesorter' );
	wp_enqueue_style( 'wow_events_admin', plugin_dir_url( __FILE__ ) . 'style/wow_events_admin.css' );
	wp_enqueue_style( 'jquery.ui.theme', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css' );

	/**
	 * hoofdmenu
	 */
	$page_title = 'WipeOut! events';
	$menu_title = 'Events';
	$capability = 'edit_pages';
	$menu_slug  = 'wow_eventsplugin';
	$function   = 'wow_eventsplugin_list';
	//$icon_url   = plugin_dir_url( __FILE__ ).'wowlogo.png';
	//$icon_url    = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/Pg0KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAyMDAxMDkwNC8vRU4iDQogImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciDQogd2lkdGg9IjM4LjAwMDAwMHB0IiBoZWlnaHQ9IjM2LjAwMDAwMHB0IiB2aWV3Qm94PSIwIDAgMzguMDAwMDAwIDM2LjAwMDAwMCINCiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWlkWU1pZCBtZWV0Ij4NCjxtZXRhZGF0YT4NCkNyZWF0ZWQgYnkgcG90cmFjZSAxLjEyLCB3cml0dGVuIGJ5IFBldGVyIFNlbGluZ2VyIDIwMDEtMjAxNQ0KPC9tZXRhZGF0YT4NCjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuMDAwMDAwLDM2LjAwMDAwMCkgc2NhbGUoMC4xMDAwMDAsLTAuMTAwMDAwKSINCmZpbGw9IiMwMDAwMDAiIHN0cm9rZT0ibm9uZSI+DQo8cGF0aCBkPSJNMTI1IDMzMSBjLTc2IC0zNSAtMTE1IC05NSAtMTE1IC0xNzcgMCAtOTkgNjQgLTE0OSAxODEgLTE0MiAzOCAyDQo3NCA5IDgwIDE1IDcgNyAtOSA5IC01OCA2IC03OSAtNSAtMTA3IDYgLTE0MSA1NyAtMjAgMjkgLTIzIDQ0IC0yMCA4OCA4IDg4DQo2MyAxNDMgMTUwIDE0OSA3MCA1IDExNCAtMjAgMTI0IC03MSA4IC00MyAxIC03MyAtMjUgLTEwNSBsLTE5IC0yNSAtOCA2MyBjLTUNCjM0IC0xMyA2MyAtMTggNjQgLTUgMSAtMjYgLTI1IC00NSAtNTggbC0zNiAtNjAgLTMgNTggYy0yIDM5IC04IDYwIC0xOCA2NA0KLTE2IDYgLTY0IC0yMiAtNjQgLTM4IDAgLTggNCAtOCAxNCAwIDE2IDE0IDI1IC0xMSAyNyAtNzQgMCAtMjIgNCAtNDkgOCAtNjANCjcgLTE3IDE0IC0xMSA0OSA0NCBsNDAgNjQgNiAtMzQgYzMgLTE5IDcgLTQ0IDggLTU3IDUgLTM5IDIxIC0yOSA3NSA0NyA1NiA3OA0KNjQgMTEwIDM3IDE1MCAtMzYgNTEgLTE1MyA2NyAtMjI5IDMyeiIvPg0KPC9nPg0KPC9zdmc+";
	$icon_url    = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBzdGFuZGFsb25lPSJubyI/Pg0KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAyMDAxMDkwNC8vRU4iDQogImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciDQogd2lkdGg9IjM4LjAwMDAwMHB0IiBoZWlnaHQ9IjM2LjAwMDAwMHB0IiB2aWV3Qm94PSIwIDAgMzguMDAwMDAwIDM2LjAwMDAwMCINCiBwcmVzZXJ2ZUFzcGVjdFJhdGlvPSJ4TWlkWU1pZCBtZWV0Ij4NCjxtZXRhZGF0YT4NCkNyZWF0ZWQgYnkgcG90cmFjZSAxLjEyLCB3cml0dGVuIGJ5IFBldGVyIFNlbGluZ2VyIDIwMDEtMjAxNQ0KPC9tZXRhZGF0YT4NCjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKDAuMDAwMDAwLDM2LjAwMDAwMCkgc2NhbGUoMC4xMDAwMDAsLTAuMTAwMDAwKSINCmZpbGw9IiM5Y2ExYTYiIHN0cm9rZT0ibm9uZSI+DQo8cGF0aCBkPSJNMTI1IDMzMSBjLTc2IC0zNSAtMTE1IC05NSAtMTE1IC0xNzcgMCAtOTkgNjQgLTE0OSAxODEgLTE0MiAzOCAyDQo3NCA5IDgwIDE1IDcgNyAtOSA5IC01OCA2IC03OSAtNSAtMTA3IDYgLTE0MSA1NyAtMjAgMjkgLTIzIDQ0IC0yMCA4OCA4IDg4DQo2MyAxNDMgMTUwIDE0OSA3MCA1IDExNCAtMjAgMTI0IC03MSA4IC00MyAxIC03MyAtMjUgLTEwNSBsLTE5IC0yNSAtOCA2MyBjLTUNCjM0IC0xMyA2MyAtMTggNjQgLTUgMSAtMjYgLTI1IC00NSAtNTggbC0zNiAtNjAgLTMgNTggYy0yIDM5IC04IDYwIC0xOCA2NA0KLTE2IDYgLTY0IC0yMiAtNjQgLTM4IDAgLTggNCAtOCAxNCAwIDE2IDE0IDI1IC0xMSAyNyAtNzQgMCAtMjIgNCAtNDkgOCAtNjANCjcgLTE3IDE0IC0xMSA0OSA0NCBsNDAgNjQgNiAtMzQgYzMgLTE5IDcgLTQ0IDggLTU3IDUgLTM5IDIxIC0yOSA3NSA0NyA1NiA3OA0KNjQgMTEwIDM3IDE1MCAtMzYgNTEgLTE1MyA2NyAtMjI5IDMyeiIvPg0KPC9nPg0KPC9zdmc+";
	$position   = null;
	add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

	/**
	 * submenu
	 */
	$parent_slug = $menu_slug;
	$page_title  = __('Add Event','wow_events');
	$menu_title  = __('Add Event','wow_events');
	$capability  = $capability;
	$menu_slug   = 'wow_add_event';
	$function    = "wow_eventsplugin_addevent";
	//$function    = array("wow_events","event_form");
	add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );

	$parent_slug = $parent_slug;
	$page_title  = __('Tour management','wow_events');
	$menu_title  = __('Tour management','wow_events');
	$capability  = $capability;
	$menu_slug   = 'tourmanagement';
	$function    = "wow_eventsplugin_tourlist";
	add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );

	$parent_slug = $parent_slug;
	$page_title  = __('Venue management','wow_events');
	$menu_title  = __('Venue management','wow_events');
	$capability  = $capability;
	$menu_slug   = 'venuemanagement';
	$function    = "wow_eventsplugin_venuelist";
	add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function );
}

