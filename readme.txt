=== Plugin Name ===
Contributors: dennislutz
Donate link: http://wipeout.nu/wordpress/
Tags: event, theatre
Requires at least: 2.0.2
Tested up to: 4.2.2
Stable tag: 1.0
License: GPLv2

Simpele plugin om events te beheren en weer te geven

== Description ==


== Installation ==

1. Upload files to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the directory of the stable readme.txt, so in this case, `/tags/4.3/screenshot-1.png` (or jpg, jpeg, gif)
2. This is the second screen shot

== Changelog ==

= 1.0 =
Initial version