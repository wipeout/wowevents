<?php
	class wowevents
	{

		/**
		 * EVENTS
		 */

		/**
		 * Add an Event
		 * @param $data array( eventdate		title		artistid		tour		ticketurl		ticketphone		note		active		createdate		updated		userid		venueid		deleted		published)
		 */
		public static function event_add( $data )
		{
			global $wpdb;
			global $eventtable;
			$now     = date("Y-m-d H:i:s");
			$user_ID = get_current_user_id();

			$newdata = array(
				'eventdate'   => $data['eventdate'],
				'title'       => $data['title'],
				'artistid'    => $data['artistid'],
				'tour'        => $data['tour'],
				'ticketurl'   => $data['ticketurl'],
				'ticketphone' => $data['ticketphone'],
				'note'        => $data['note'],
				'active'      => $data['active'],
				'createdate'  => $now,
				'updated'     => $now,
				'userid'      => $user_ID,
				'venueid'     => $data['venueid'],
				'deleted'     => 0,
				'published'   => $data['published'],
			);

			$wpdb->insert( $eventtable, $newdata );
			return $wpdb->insert_id;
		}


		/**
		 * Mark an event 'deleted', optionally really delete id
		 * @param $eventid
		 * @param bool $force_delete
		 */
		public static function event_delete( $eventid, $force_delete = false )
		{
			global $wpdb;
			global $eventtable;

			if( $force_delete )
				$wpdb->delete( $eventtable, array( 'eventid' => $eventid ), array( '%d' ) );
		}

		public static function event_toggle_delete( $eventid )
		{
			global $wpdb;
			global $eventtable;
			$sql = "UPDATE {$eventtable} SET deleted = ! deleted WHERE eventid=%s";
			$wpdb->query(
				$wpdb->prepare($sql,$eventid)
			);
		}

		public static function event_toggle_active( $eventid )
		{
			global $wpdb;
			global $eventtable;
			$sql = "UPDATE {$eventtable} SET active = ! active WHERE eventid=%s";
			$wpdb->query(
				$wpdb->prepare($sql,$eventid)
			);
		}

		public static function event_delete_from_tour() {}

		public static function event_edit( $eventid, $data )
		{
			global $wpdb;
			global $eventtable;

			$now     = date("Y-m-d H:i:s");
			$user_ID = get_current_user_id();

			$newdata = array(
				'eventdate'   => $data['eventdate'],
				'title'       => $data['title'],
				'artistid'    => $data['artistid'],
				'tour'        => $data['tour'],
				'ticketurl'   => $data['ticketurl'],
				'ticketphone' => $data['ticketphone'],
				'note'        => $data['note'],
				'active'      => $data['active'],
				'updated'     => $now,
				'userid'      => $user_ID,
				'venueid'     => $data['venueid'],
				'deleted'     => $data['deleted'],
				'published'   => $data['published'],
				'eventid'     => $eventid,
			);

			$sql = "
				UPDATE {$eventtable}
				SET
					'eventdate'   = %d
					'title'       = %d
					'artistid'    = %s
					'tour'        = %d
					'ticketurl'   = %d
					'ticketphone' = %d
					'note'        = %d
					'active'      = %s
					'updated'     = %d
					'userid'      = %s
					'venueid'     = %s
					'deleted'     = %s
					'published'   = %s
				WHERE eventid = %s
			";

			$wpdb->query(
				$wpdb->prepare($sql,$newdata)
			);
		}

		/**
		 * List events
		 * @param array $options
		 *
		 * @return mixed
		 */
		public static function event_list( $options = array( "admin"=>0 ) )
		{
			global $wpdb;
			global $eventtable;
			global $artisttable;
			global $venuetable;
			global $tourtable;
			global $toureventtable;
			$admin = $options["admin"] ? "" : " AND active=1";
			$array = $options["array"] ? true : false;

			$sql = "
				SELECT
					e.eventid,e.eventdate,e.title,e.artistid,e.ticketurl,
					e.ticketphone,e.note,e.active,e.createdate,e.updated,e.userid,e.venueid,e.deleted,e.published,
					a.name AS artistname, a.url AS artisturl,
					v.name AS venuename, v.address AS venueaddress, v.city AS venuecity, v.zip AS venuezip,
					t.name AS tourname
				FROM {$eventtable} AS e
					LEFT JOIN {$artisttable} AS a USING (artistid)
					LEFT JOIN {$venuetable} AS v USING(venueid)
					LEFT JOIN {$toureventtable} AS te ON(te.eventid=e.eventid)
					LEFT JOIN {$tourtable} AS t ON(t.tourid = te.tourid)
				WHERE e.deleted=0{$admin}
				ORDER BY e.eventdate
			";
			if( $array )
				$events = $wpdb->get_results( $sql,ARRAY_A );
			else
				$events = $wpdb->get_results( $sql );

			if( $options['eventid'] > 0 )
			{
				foreach( $events as $e )
				{
					if( $array )
						if( $e['eventid'] == $options['eventid'] ) return $e;
					else
						if( $e->eventid == $options['eventid'] ) return $e;
				}
			}
			return $events;
		}

		public static function event_info( $eventid, $array = 1 )
		{
			$event = self::event_list( array( "eventid" =>$eventid, "array" => $array ) );
			return $event;
		}

		public static function event_form( $eventid=0 )
		{

			echo "
				<div class='form eventform'>
					<h2>".translate("Add event","wow_events")."</h2>
					<div><label><input type=\"text\" name=\"event_date\"   id=\"c_eventdate\"   value=\"".$data['date']."\"   placeholder=\"". __( "Date", "wow_events" )."\"/></label></div>
					<div><label><input type=\"text\" name=\"event_title\"  id=\"c_eventtitle\"  value=\"".$data['title']."\"  placeholder=\"". __( "Title", "wow_events" )."\"/></label></div>
					<div><label><input type=\"text\" name=\"event_artist\" id=\"c_eventartist\" value=\"".$data['artist']."\" placeholder=\"". __("Artist","wow_events")."\"/></label></div>
					<div><label><input type=\"text\" name=\"event_venue\"  id=\"c_eventvenue\"  value=\"".$data['venue']."\"  placeholder=\"". __("Venue","wow_events")."\"/></label></div>
					<div><label><input type=\"text\" name=\"event_tour\"   id=\"c_eventtour\"   value=\"".$data['tour']."\"   placeholder=\"". __("Tour","wow_events")."\"/></label></div>

					<div><label><input type=\"text\" name=\"event_ticketurl\"   id=\"c_event_ticket_url\"   value=\"".$data['ticket_url']."\"   placeholder=\"". __("Ticket url","wow_events")."\"/></label></div>
					<div><label><input type=\"text\" name=\"event_ticketphone\" id=\"c_event_ticket_phone\" value=\"".$data['ticket_phone']."\" placeholder=\"". __("Ticket phone","wow_events")."\"/></label></div>

					<div><label><textarea name=\"event_note\" id=\"c_event_note\" placeholder=\"".__("Note","wow_events")."\">".$data['note']."</textarea></label></div>

					<div><label>".__("Active","wow_events")."<input type=\"checkbox\" name=\"active\"/></label></div>
					<div><label>".__("Published","wow_events")."<input type=\"checkbox\" name=\"published\"/></label></div>
					<div>
						<button type=\"button\" name=\"event_submit\" id=\"c_submit\" class=\"button-secondary action\" disabled=\"disabled\">". __( "Add Event", "wow_events" )."</button>
					</div>
				</div>
			";
		}

		
		/**
		 * ARTISTS
		 */

		public static function artist_add() {}
		public static function artist_add_to_event() {}
		public static function artist_delete() {}
		public static function artist_delete_from_Event() {}
		public static function artist_edit() {}
		public static function artist_list() {}

		/**
		 * TOURS
		 */

		public static function tour_add() {}
		public static function tour_add_to_event() {}
		public static function tour_delete() {}
		public static function tour_delete_from_Event() {}
		public static function tour_edit() {}
		public static function tour_list() {}

		/**
		 * VENUES
		 */

		/**
		 * Add a venue
		 * @param $data
		 */
		public static function venue_add( $data )
		{
			global $venuetable;
			global $wpdb;

			$newdata = array(
				'name'      => $data['name'],
				'address'   => $data['address'],
				'zip'       => $data['zip'],
				'city'      => $data['city'],
				'phone'     => $data['phone'],
				'url'       => $data['url'],
				'createdate'=> $now,
				'userid'    => $user_ID
			);
			$wpdb->insert( $venuetable, $newdata );
		}

		public static function venue_delete() {}
		public static function venue_edit() {}
		public static function venue_list() {}
	}