<?php
	global $wow_events_db_version;
	$wow_events_db_version = "1.0";

	function wow_events_install() {
		global $wpdb;
		global $wow_events_db_version;
		$user_ID = get_current_user_id();
		$now     = date("Y-m-d H:i:s");
		//$wpdb->show_errors();

		// Bepaal de tabelnamen
		$eventtable       = $wpdb->prefix . "wowevents_events";
		$artisttable      = $wpdb->prefix . "wowevents_artists";
		$venuetable       = $wpdb->prefix . "wowevents_venues";
		$tourtable        = $wpdb->prefix . "wowevents_tour";
		$tour_eventtable  = $wpdb->prefix . "wowevents_tour_events";

		// Definieer de tabellen
		$eventtable_sql = "
			CREATE TABLE $eventtable (
				eventid MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
				eventdate DATE NOT NULL,
				title VARCHAR(55) NOT NULL,
				artistid MEDIUMINT(9) NOT NULL,
				tour VARCHAR(55) NOT NULL,
				ticketurl VARCHAR(55) NOT NULL,
				ticketphone VARCHAR(25) NOT NULL,
				note TEXT DEFAULT '',
				active TINYINT(1) DEFAULT 1,
				createdate DATETIME NOT NULL,
				updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				userid MEDIUMINT(9) NOT NULL,
				venueid MEDIUMINT(9) NOT NULL,
				deleted TINYINT(1) DEFAULT 0,
				published TINYINT(1) DEFAULT 1,
				PRIMARY KEY (eventid),
				UNIQUE KEY titleartist (title,artistid),
				KEY eventdate (eventdate),
				KEY title (title)
			) DEFAULT CHARSET=utf8;
		";

		$artisttable_sql = "
			CREATE TABLE $artisttable (
				artistid MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
				name VARCHAR(55) NOT NULL,
				url VARCHAR(55) NOT NULL,
				bio TEXT DEFAULT '',
				createdate DATETIME NOT NULL,
				updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				userid MEDIUMINT(9) NOT NULL,
				PRIMARY KEY (artistid)
			) DEFAULT CHARSET=utf8;
		";

		$venuetable_sql = "
			CREATE TABLE $venuetable (
				venueid MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
				name VARCHAR(55) NOT NULL,
				address VARCHAR(55) NOT NULL,
				zip VARCHAR(15) NOT NULL,
				city VARCHAR(55) NOT NULL,
				phone VARCHAR(55) NOT NULL,
				url VARCHAR(55) NOT NULL,
				createdate DATETIME NOT NULL,
				updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				userid MEDIUMINT(9) NOT NULL,
				deleted TINYINT(1) DEFAULT 0,
				PRIMARY KEY (venueid)
			) DEFAULT CHARSET=utf8;
		";

		$tourtable_sql = "
			CREATE TABLE $tourtable (
				tourid MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
				name VARCHAR(55) NOT NULL,
				note TEXT DEFAULT '',
				createdate DATETIME NOT NULL,
				updated TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				userid MEDIUMINT(9) NOT NULL,
				PRIMARY KEY (tourid),
				KEY name (name)
			) DEFAULT CHARSET=utf8;
		";

		$tour_event_sql = "
		CREATE TABLE $tour_eventtable (
			tourid int(11) NOT NULL,
			eventid int(11) NOT NULL,
			PRIMARY KEY (tourid,eventid)
		) DEFAULT CHARSET=utf8
		";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		/**
		 * Tabellen aanmaken
		 */
		dbDelta( $eventtable_sql );
		dbDelta( $artisttable_sql );
		dbDelta( $venuetable_sql );
		dbDelta( $tourtable_sql );
		dbDelta( $tour_event_sql );

	 /**
	 * Voeg initiele data toe.
	 */
		$data = array(
			'name'      =>'Koninklijk Theater Carré',
			'address'   =>'Amstel 115-125',
			'zip'       =>'1018 EM',
			'city'      =>'Amsterdam',
			'phone'     =>'0900 2525255',
			'url'       =>'http://www.carre.nl/',
			'createdate'=>$now,
			'userid'    =>$user_ID
		);
		$wpdb->insert( $venuetable, $data );

		$data = array(
			'name'      =>'Theater PePijn',
			'address'   =>'Nieuwe Schoolstraat 21-23',
			'zip'       =>'2514 HT',
			'city'      =>'Den Haag',
			'phone'     =>'070 361 0540',
			'url'       =>'http://www.diligentia-pepijn.nl/',
			'createdate'=>$now,
			'userid'    =>$user_ID
		);
		$wpdb->insert( $venuetable, $data );

		/**
		 * Registreer de databaseversie
		 */
		add_option("wow_events_db_version", $wow_events_db_version);
	}

