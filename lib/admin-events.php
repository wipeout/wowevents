<?php
	/**
	 * html, forms, javascript en ajax-afhandeling voor adminpagina
	 */

	/**
	 * Pagina met eventformulier
	 */
	function wow_eventsplugin_addevent()
	{
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( "You do not have sufficient permissions to access this page.", "wow_events" ) );
		}
		wowevents::event_form();
		print_r( wowevents::event_info(1) );
	}

	/**
	 * Pagina met eventslijst
	 */
	function wow_eventsplugin_list()
	{
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( "You do not have sufficient permissions to access this page.", "wow_events" ) );
		}
		$options = array( "admin" => 1 );
		$events  = wowevents::event_list( $options );
		?>
		<div class="wrap">
			<h2><?php _e( "Events", "wow_events" );?></h2>
			<table id="events" class="wp-list-table widefat fixed posts tablesorter">
				<colgroup>
					<col class="col-eventdate"/>
					<col class="col-title"/>
					<col class="col-artist"/>
					<col class="col-venue"/>
					<col class="col-tour"/>
					<col class="col-active"/>
					<col class="col-published"/>
				</colgroup>
				<thead>
					<tr>
						<th><?php _e( "Date", "wow_events" );?></th>
						<th><?php _e( "Title", "wow_events" );?></th>
						<th><?php _e( "Artist", "wow_events" );?></th>
						<th><?php _e( "Venue", "wow_events" );?></th>
						<th><?php _e( "Tour", "wow_events" );?></th>
						<th class="not-sortable"><?php _e( "Active", "wow_events" );?></th>
						<th class="not-sortable"><?php _e( "Show", "wow_events" );?></th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th><?php _e( "Date", "wow_events" );?></th>
						<th><?php _e( "Title", "wow_events" );?></th>
						<th><?php _e( "Artist", "wow_events" );?></th>
						<th><?php _e( "Venue", "wow_events" );?></th>
						<th><?php _e( "Tour", "wow_events" );?></th>
						<th class="not-sortable"><?php _e( "Active", "wow_events" );?></th>
						<th class="not-sortable"><?php _e( "Show", "wow_events" );?></th>
					</tr>
				</tfoot>
				<tbody>
				<?php
					foreach ( $events as $s )
					{
						$trclass  = "";
						$achecked = "";
						$pchecked = "";

						$userdata = get_userdata( $s->userid );
						if ( ! $s->published ) {
							$trclass = " unpublished";
						} else {
							$pchecked = "checked = \"checked\"";
						}
						if ( ! $s->active ) {
							$trclass .= " inactive";
						} else {
							$achecked = "checked = \"checked\"";
						}
						echo '
							<tr id="event_' . $s->eventid . '" class="eventrow' . $trclass . '">
								<td class="date">' . date_i18n( 'j F Y', strtotime( $s->date ) ) . ' <br/><span class="row-actions delete"> <a href="#" class="submitdelete">' . __( "Delete", "wow_events" ) . '</a></span> <span class="row-actions">|</span> <span class="row-actions edit"> <a href="#" class="submitedit">' . __( "Edit", "wow_events" ) . '</a></span></td>
								<td class="title">' . $s->title . '</td>
								<td class="artist"><span class="artistname">' . $s->artistname . '</span></td>
								<td class="venue"><span class="venue">' . $s->venuename . '</span><br/><span class="venuecity">' . $s->venuecity . '</span></td>
								<td class="tour"><span class="tour">' . $s->tourname . '</span></td>
								<td class="active"><input type="checkbox" name="active" class="toggleactive" ' . $achecked . '/></td>
								<td class="published"><input type="checkbox" name="published" class="togglepublished" ' . $pchecked . '/></td>
							</tr>';
					}
				?>
				</tbody>
			</table>


		</div> <!-- /wrap -->
	<?php }